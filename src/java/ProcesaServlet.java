

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/ProcesaServlet"})
public class ProcesaServlet extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String texto1 = request.getParameter("texto1");
        String texto2 = request.getParameter("texto2");
        
        Comparacion comparacion = new Comparacion();
        int percentage = comparacion.smart_match(texto1, texto2);
        
        request.setAttribute("percentage", texto2);
        
        request.getRequestDispatcher("response.jsp").forward(request, response);
        
    }
}
